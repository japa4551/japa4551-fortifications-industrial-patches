using RimWorld;
using UnityEngine;
using Verse;

namespace FortificationIndustrialPatches
{
    public class IsDecorationModExtension : DefModExtension {}
	
	public class Designator_DeconstructFloorDeco : Designator_Deconstruct
	{
		public static DesignationDef DeconstructFloorDecoDesignator;
		public Designator_DeconstructFloorDeco()
		{
			this.defaultLabel = "Deconstruct Citadel Decorations";
			this.icon = ContentFinder<Texture2D>.Get("UI/Designators/DeconstructFloorDeco", true);
			this.hotKey = null;
		}

		public override AcceptanceReport CanDesignateThing(Thing t)
		{
			var building = t;

			if (building != null) {
				var is_valid_building = building.def.HasModExtension<IsDecorationModExtension>();
				var can_designate = base.CanDesignateThing(building).Accepted;

				if (is_valid_building && can_designate) return true;
			}

			return false;
		}
    }
}
