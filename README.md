And why would you read me?

Adds Compatibility with other mods and a few other things.

More Details on the [Steam Workshop Page](https://steamcommunity.com/sharedfiles/filedetails/?id=3161149745)

Special thanks to all the people that helped me on the RimWorld Discord, mainly those four wonderful persons: aelana, steveo.o, espioidsavant and erdelf.